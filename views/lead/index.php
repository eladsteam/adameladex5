<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
		<?php if (\Yii::$app->user->can('createLead')) { //hide this button from who is not authorized ?>
        <?= Html::a('Create Lead', ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'email:email',
            'phone',
            'notes:ntext',
           // 'status',
			[
				'attribute' => 'status',
				'label' => 'Status',
				'format' => 'raw',
				'value' => function($model){
					return Html::a($model->statusItem->name, //The getStatusItem function connecting the status table to her self and convert id status to name status
					['lead/view', 'id' => $model->id]);  //Show the leas view page by id 
				},
				'filter'=>Html::dropDownList('LeadSearch[status]', $status, $statuses, ['class'=>'form-control']), // status and statuses came from model/status
											//LeadSearch[status] for display all statuses => we need the line with the -1 in the leadSearch.php
			],
			
			[
				'attribute' => 'owner',
				'label' => 'Owner',
				'format' => 'raw',
				'value' => function($model){
					return Html::a($model->userOwner->fullname, 
					['user/view', 'id' => $model->userOwner->id]);
				},
				'filter'=>Html::dropDownList('LeadSearch[owner]', $owner, $owners, ['class'=>'form-control']),
											//LeadSearch[owner] for display all Owner => we need the line with the -1 in the leadSearch.php
				],
            //'owner',	
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
