<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Status;



/* @var $this yii\web\View */
/* @var $model app\models\Lead */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lead-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?>  

	<?php if($model->isNewRecord){ //show owner field only if isNewRecord activate => if we don't try to update lead, only creating a new one ?>
		<?php if (\Yii::$app->user->can('createLead')) { //hide this button from who is not authorized => only the Team Leader or Admin can! ?>
			<?= $form->field($model, 'owner')->
						dropDownList(User::getUsers()) ?>   
		<?php } ?>
	<?php } //end of isNewRecord activate ?>
	
<!------ hide this 4 fields from create and update
    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>	 
----->
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
