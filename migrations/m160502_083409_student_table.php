<?php

use yii\db\Migration;

class m160502_083409_student_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'student',
		[
			'id' => 'pk',
			'firstName' => 'string',
			'lastName' => 'string',
		],
		'ENGINE=InnoDB'
		);
    }
    public function down()
    {
       $this->dropTable('student') ;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
